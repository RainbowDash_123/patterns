// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Patterns/AbstarctFactory/Loot/Loot.h"
#include "NecroLoot.generated.h"

/**
 * An element of necro armor
 */
UCLASS()
class PATTERNS_API ANecroArmor : public ALoot, public FArmorInfo //for some reason, private inheritance is not allowed in the UE, so public
{
	GENERATED_BODY()

public:
	virtual void StoreItem() override {/*Implementation*/ };
	virtual void UseItem() override {/*Implementation*/ };
	virtual void SetupWithInfo(FBaseInfo* InfoPtr) override;
};

inline void ANecroArmor::SetupWithInfo(FBaseInfo* InfoPtr)
{
	if (const auto ArmorInfo = static_cast<FArmorInfo*>(InfoPtr))
	{
		Durability = ArmorInfo->Durability;
	}
}

/**
 * An element of necro weapon
 */
UCLASS()
class PATTERNS_API ANecroWeapon : public ALoot, public FWeaponInfo
{
	GENERATED_BODY()

public:
	virtual void StoreItem() override {/*Implementation*/ };
	virtual void UseItem() override {/*Implementation*/ };
	virtual void SetupWithInfo(FBaseInfo* InfoPtr) override;

};

inline void ANecroWeapon::SetupWithInfo(FBaseInfo* InfoPtr)
{
	if (const auto WeaponInfo = static_cast<FWeaponInfo*>(InfoPtr))
	{
		Damage = WeaponInfo->Damage;
		Durability = WeaponInfo->Durability;
	}
}