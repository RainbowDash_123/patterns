// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CityGenerator.h"
#include "GameFramework/Actor.h"
#include "CanGenerateCity.h"
#include "GenerationDirector.h"
#include "MapGenerator.generated.h"

/*
 * Some actor that is the centre point for all cities to be built around.
 */
UCLASS()
class PATTERNS_API AMapGenerator : public AActor
{
	GENERATED_BODY()

public:
	/** Creates two cities and quests for them */
	void GenerateMap();

private:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Cities", meta = (AllowPrivateAccess = "true"))
		TArray<AActor*> Cities;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Quests", meta = (AllowPrivateAccess = "true"))
		TArray<UObject*> Quests;
};

template<class TClass>
ICanGenerateCity* CastToCanGenerateCity(TClass* Object)
{
	return static_cast<ICanGenerateCity*>(Object->GetInterfaceAddress(UCanGenerateCity::StaticClass()));
}

inline void AMapGenerator::GenerateMap()
{
	//Creating necromantic city
	auto NecroGenerator = NewObject<UNecroCityGenerator>(this);
	auto IGenerator = CastToCanGenerateCity(NecroGenerator);
	if (IGenerator) 
	{
		UGenerationDirector::GenerateNecroCity(IGenerator);
		Cities.Emplace(Cast<AActor>(NecroGenerator->GetResult()));
	}
	
	//Create necromantic city quests
	auto QuestGenerator = NewObject<UCityQuestGenerator>(this);
	IGenerator = CastToCanGenerateCity(QuestGenerator);
	if(IGenerator)
	{
		UGenerationDirector::GenerateNecroCity(IGenerator);
		Quests.Emplace(QuestGenerator->GetResult());
	}	
	
	//Create elven city
	auto ElvenGenerator = NewObject<UElvenCityGenerator>(this);
	IGenerator = CastToCanGenerateCity(ElvenGenerator);
	if(IGenerator)
	{
		UGenerationDirector::GenerateElvenCity(IGenerator);
		Cities.Emplace(Cast<AActor>(ElvenGenerator->GetResult()));
	}

	//Create elven city quests
	IGenerator = CastToCanGenerateCity(QuestGenerator);
	if(IGenerator)
	{
		UGenerationDirector::GenerateElvenCity(IGenerator);
		Quests.Emplace(QuestGenerator->GetResult());
	}	
}