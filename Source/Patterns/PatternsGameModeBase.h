// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PatternsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PATTERNS_API APatternsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
